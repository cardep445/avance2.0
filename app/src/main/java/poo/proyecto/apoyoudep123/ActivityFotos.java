package poo.proyecto.apoyoudep123;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import poo.proyecto.apoyoudep123.db.Archivo;
import poo.proyecto.apoyoudep123.db.Conexion;

public class ActivityFotos extends AppCompatActivity implements  View.OnClickListener, AdapterView.OnItemClickListener  {

    Conexion conexion;

    ImageButton botonEditar;
    ImageView imagen;

    byte[] blob;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fotos);

        // recibo el objeto serializado
        Bundle extras = getIntent().getExtras();
        Archivo usuario = (Archivo)extras.getSerializable("foto");

        Conexion conexion= new Conexion(this);

        botonEditar = (ImageButton) findViewById(R.id.bregresar);
        botonEditar.setOnClickListener(this);

        imagen = (ImageView)findViewById(R.id.imageView2);
        imagen.setOnClickListener(this);

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Archivo.CAMPO_FOTO};
        Cursor cursor = db.query(Archivo.TABLA_ARCHIVO, campos, null,null,null,null,null);
        cursor.moveToFirst();

        if (cursor.getBlob(0) != null) {
            blob = cursor.getBlob(0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(blob,0,blob.length);
            imagen.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.bregresar) {

            Intent intent = new Intent(this, ActivityBMaterial.class);
            startActivity(intent);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}