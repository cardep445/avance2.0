package poo.proyecto.apoyoudep123;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import poo.proyecto.apoyoudep123.db.Usuario;


public class Activity6 extends AppCompatActivity implements View.OnClickListener {
    ImageButton bregresar;
    EditText NombreAlumno, FacultadAlumno, CorreoAlumno, UsuarioAlumno;
    Button bContrasena , botoncambiar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_6);

        bregresar=(ImageButton)findViewById(R.id.botonRetroceso);
        bregresar.setOnClickListener(this);

        NombreAlumno = findViewById(R.id.nombreAlumno);
        FacultadAlumno = findViewById(R.id.facultadAlumno);
        CorreoAlumno = findViewById(R.id.correoAlumno);
        UsuarioAlumno = findViewById(R.id.usuarioAlumno);

        NombreAlumno.setText(Activity5.Nombre);
        FacultadAlumno.setText(Activity5.Facultad);
        UsuarioAlumno.setText(Activity5.Usu);
        CorreoAlumno.setText(Activity5.Correo);

        botoncambiar=findViewById(R.id.bCambiarContrasena);
        botoncambiar.setOnClickListener(this);

        /*Bundle extras = getIntent().getExtras();
        if(extras==null) {
        }

        if (extras!=null) {
            Usuario id = (Usuario)extras.getSerializable("id");


            NombreAlumno.setText(id.getNombre());
            FacultadAlumno.setText(id.getFacultad());
            UsuarioAlumno.setText(id.getUsuario());
            CorreoAlumno.setText(id.getCorreo());

         */

    }


    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.botonRetroceso){
            Intent i = new Intent(this, MainActivityfalse.class);
            startActivity(i);
        }
        if (v.getId() == R.id.bCambiarContrasena) {
            Intent i = new Intent (this, ActivityContraPerfil.class);
            startActivity(i);
        }

    }
}