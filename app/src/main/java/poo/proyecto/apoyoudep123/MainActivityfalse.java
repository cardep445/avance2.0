package poo.proyecto.apoyoudep123;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivityfalse extends AppCompatActivity implements View.OnClickListener{
    Button bsubirArchivos, bverArchivos, bperfil;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_false);


        bsubirArchivos=(Button)findViewById(R.id.bsubirArchivos);
        bsubirArchivos.setOnClickListener(this);

        bverArchivos=(Button)findViewById(R.id.bverArchivos);
        bverArchivos.setOnClickListener(this);

        bperfil=(Button)findViewById(R.id.bperfil);
        bperfil.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.bsubirArchivos) {
            Intent i = new Intent("android.intent.action.Activity2");
            startActivity(i);
        }
        if (v.getId()==R.id.bverArchivos) {
            Intent i = new Intent("android.intent.action.Activity4");
            startActivity(i);
        }
        if (v.getId()==R.id.bperfil) {
            Intent i = new Intent("android.intent.action.Activity6");
            startActivity(i);
        }
    }
}
