package poo.proyecto.apoyoudep123;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import poo.proyecto.apoyoudep123.db.Archivo;
import poo.proyecto.apoyoudep123.db.Conexion;

public class ActivityBPcs extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener{
    ListView listViewPcs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b_pcs);

        listViewPcs = findViewById(R.id.listViewPcs);
        listViewPcs.setOnItemClickListener(this);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getLista());
        listViewPcs.setAdapter(adaptador);
    }
    Conexion conexion = new Conexion(this);
    List<Archivo> listaArchivos;

    private List<String> getLista() {

        listaArchivos = new LinkedList<Archivo>();
        List<String> listaNombres = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Archivo.CAMPO_NOMBRE, Archivo.CAMPO_CURSO, Archivo.CAMPO_CODIGODETIPO, Archivo.CAMPO_FOTO};

        Cursor cursor = db.query(Archivo.TABLA_ARCHIVO,campos,null,null,null,null,null);
        while (cursor.moveToNext()) {

            String nombre = cursor.getString(0);
            String curso = cursor.getString(1);
            int codigodetipo = cursor.getInt(2);

            Bitmap bitmap = null;
            if (cursor.getBlob(3)!=null) {
                byte[] bytes = cursor.getBlob(3);
                bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            }

            Archivo u = new Archivo(nombre,curso,codigodetipo,bitmap);
            listaArchivos.add(u);
            db.close();

            listaNombres.add(u.getNombre());

        }

        return listaNombres;

    }
    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}