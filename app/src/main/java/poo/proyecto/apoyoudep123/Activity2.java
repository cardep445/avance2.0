package poo.proyecto.apoyoudep123;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import poo.proyecto.apoyoudep123.db.Archivo;
import poo.proyecto.apoyoudep123.db.Conexion;

public class Activity2 extends AppCompatActivity implements View.OnClickListener {

    Button bterminar, bcancelar, bsubirarchivo;
    ProgressBar barraSubirArchivo;
    int contador = 0 , marcadorDeTipo=0 ;
    ImageView imageView;
    Spinner spinner;
    EditText nombreArchivo, nombreCurso;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        nombreArchivo=findViewById(R.id.nombreArchivo);
        nombreCurso=findViewById(R.id.nombreCurso);

        barraSubirArchivo=(ProgressBar) findViewById(R.id.progressBarSubirArchivo);

        bterminar = (Button)findViewById(R.id.bterminar);
        bterminar.setOnClickListener(this);

        bcancelar = (Button)findViewById(R.id.bcancelar);
        bcancelar.setOnClickListener(this);

        bsubirarchivo=(Button)findViewById(R.id.bsubirArchivo);
        bsubirarchivo.setOnClickListener(this);



        spinner=(Spinner)findViewById(R.id.curso2);
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this, R.array.archivos,android.R.layout.simple_spinner_item);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equals("Resumen")){
                    marcadorDeTipo=1;
                }
                if (parent.getItemAtPosition(position).toString().equals("Material")){
                    marcadorDeTipo=2;
                }
                if (parent.getItemAtPosition(position).toString().equals("PC´s pasadas")){
                    marcadorDeTipo=3;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imageView=(ImageView) findViewById(R.id.imageViewsubirresumen);

    }

    public void prog() {
        final Timer t = new Timer();
        TimerTask tt = new TimerTask()
        {
            @Override
            public void run() {
                contador=contador +10;
                barraSubirArchivo.setProgress(contador);
                if (contador==100){
                    t.cancel();
                }

            }
        };
        t.schedule(tt,0,100);
    }


    @Override
    public void onClick(View v){
        if (v.getId()==R.id.bterminar){
            Conexion conexion=new Conexion(this);
            SQLiteDatabase db = conexion.getWritableDatabase();
            ContentValues values=new ContentValues();

            if (foto!=null){
                byte[] blob;
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                this.foto.compress(Bitmap.CompressFormat.JPEG,100,out);

                blob=out.toByteArray();

                values.put(Archivo.CAMPO_FOTO,blob);
            }

            values.put(Archivo.CAMPO_CURSO,nombreCurso.getText().toString());
            values.put(Archivo.CAMPO_NOMBRE,nombreArchivo.getText().toString());
            values.put(Archivo.CAMPO_CODIGODETIPO,marcadorDeTipo);

            db.insert(Archivo.TABLA_ARCHIVO,Archivo.CAMPO_NOMBRE, values);


            Intent i = new Intent(this, Activity3.class);
            startActivity(i);
            db.close();
        }

        if (v.getId()==R.id.bcancelar){
            Intent i = new Intent(this, MainActivityfalse.class);
            startActivity(i);
        }



        if (v.getId()==R.id.bsubirArchivo) {

            if (marcadorDeTipo==1) {

                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    String[] permisos = {Manifest.permission.READ_EXTERNAL_STORAGE};
                    ActivityCompat.requestPermissions(Activity2.this, permisos, REQUEST_CODE_STORAGE_PERMISSION);

                } else {

                    seleccinarImagen();
                }
            }

            if (marcadorDeTipo==2){
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    String[] permisos = {Manifest.permission.READ_EXTERNAL_STORAGE};
                    ActivityCompat.requestPermissions(Activity2.this, permisos, REQUEST_CODE_STORAGE_PERMISSION);

                } else {

                    seleccinarImagen();
                }

            }

            if (marcadorDeTipo==3){
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    String[] permisos = {Manifest.permission.READ_EXTERNAL_STORAGE};
                    ActivityCompat.requestPermissions(Activity2.this, permisos, REQUEST_CODE_STORAGE_PERMISSION);

                } else {

                    seleccinarImagen();
                }

            }

            if (marcadorDeTipo ==0){
                Toast.makeText(this, "Debe seleccionar un tipo de archivo", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static final int REQUEST_CODE_STORAGE_PERMISSION = 1;
    private static final int REQUEST_CODE_SELECT_IMAGE = 2;

    private void seleccinarImagen(){
        Intent intent= new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity(getPackageManager())!= null){
            startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode==REQUEST_CODE_STORAGE_PERMISSION && grantResults.length>0){

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                seleccinarImagen();
                prog();
            }else{
                Toast.makeText(this, "Permiso Denegado", Toast.LENGTH_LONG);
            }
        }
    }

    Bitmap foto;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK){

            if (data!= null && data.getData()!=null){

                try {

                    Uri selectImageUri = data.getData();
                    InputStream in = getContentResolver().openInputStream(selectImageUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(in);
                    imageView.setImageBitmap(bitmap);

                    this.foto=bitmap;

                } catch (FileNotFoundException e){

                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
        }
    }
}
