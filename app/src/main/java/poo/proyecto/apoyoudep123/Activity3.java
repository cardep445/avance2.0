package poo.proyecto.apoyoudep123;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


public class Activity3 extends AppCompatActivity implements View.OnClickListener {
    Button bsubirOtroArchivo, bpaginaPrincipal;
    int x=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);

        bsubirOtroArchivo=(Button)findViewById(R.id.bsubirOtroArchivo);
        bsubirOtroArchivo.setOnClickListener(this);

        bpaginaPrincipal=(Button)findViewById(R.id.bpaginaPrincipal);
        bpaginaPrincipal.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.bsubirOtroArchivo) {
            Intent i = new Intent("android.intent.action.Activity2");
            startActivity(i);
        }
        if(v.getId()==R.id.bpaginaPrincipal) {

            Intent i = new Intent(this, MainActivityfalse.class);
            startActivity(i);

        }
    }
}
