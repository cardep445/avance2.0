package poo.proyecto.apoyoudep123;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import poo.proyecto.apoyoudep123.db.Conexion;
import poo.proyecto.apoyoudep123.db.Usuario;


public class Activity5 extends AppCompatActivity implements  View.OnClickListener {
    public static String Nombre, Facultad, Correo, Usu, contra;
    EditText CampoUsuario, CampoContrasena;
    TextView nombre, usuario, contrasena, inicioPerfil;
    ImageView udep;
    Button botonISesion, botonRegistro, botonRegistro2;
    int i = 0;
    ListView listView1;
    Bundle extras;
    Usuario id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);


        CampoUsuario = findViewById(R.id.campoUsuario);
        CampoContrasena = findViewById(R.id.campoContrasena);
        usuario = findViewById(R.id.usuario);
        contrasena = findViewById(R.id.contrasena);

        udep = findViewById(R.id.udep);
        botonISesion = (Button) findViewById((R.id.botonISesion));
        botonISesion.setOnClickListener(this);

        botonRegistro = (Button) findViewById(R.id.botonRegistro);
        botonRegistro.setOnClickListener(this);

        botonRegistro2=findViewById(R.id.botonRegistro2);
        botonRegistro2.setOnClickListener(this);

    }


    Conexion conexion = new Conexion(this);

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.botonISesion) {
            String nombreusuario = CampoUsuario.getText().toString();
            String clave = CampoContrasena.getText().toString();

            SQLiteDatabase db = conexion.getReadableDatabase();
            String[] campos = {Usuario.CAMPO_USUARIO, Usuario.CAMPO_CONTRASENA, Usuario.CAMPO_CORREO, Usuario.CAMPO_FACULTAD, Usuario.CAMPO_NOMBRE};

            Cursor cursor = db.query(Usuario.TABLA_USUARIOS, campos, null,
                    null, null, null, null);

            while (cursor.moveToNext()) {

                String u = cursor.getString(0);
                String c = cursor.getString(1);
                String crr = cursor.getString(2);
                String f = cursor.getString(3);
                String n = cursor.getString(4);

                if (u.equals(nombreusuario) && c.equals(clave)) {

                    Usu = u;
                    Nombre = n;
                    Facultad = f;
                    Correo = crr;
                    contra = c;

                    Intent intent = new Intent(this, MainActivityfalse.class);
                    startActivity(intent);

                    /*
                    Intent j = new Intent(this, Activity6.class);

                    Usuario id = new Usuario(u, c, crr, f, n);

                    Bundle extras = new Bundle();
                    extras.putSerializable("id",id);
                    j.putExtras(extras);
                    */

                }else {
                    Toast.makeText(this, getString(R.string.falloInicioS), Toast.LENGTH_SHORT).show();
                }
            }
        }



        if (v.getId() == R.id.botonRegistro) {
            Intent j = new Intent(this, Activity7.class);
            startActivity(j);
        }

        if (v.getId() == R.id.botonRegistro2) {
            Intent e = new Intent(this, ActivityContrasena.class);
            startActivity(e);
        }

    }
}

