package poo.proyecto.apoyoudep123.db;

import android.graphics.Bitmap;

import java.io.Serializable;

public class Archivo implements Serializable {
    String nombre, curso;
    int codigoDeTipo;  // // 0=Resumen ; 1=Material ; 2=PcsPasadas
    private transient Bitmap bitmap;

    public Archivo(String nombre, String curso, int codigo,  Bitmap bitmap) {
        this.nombre = nombre;
        this.curso = curso;
        this.codigoDeTipo = codigo;
        this.bitmap=bitmap;
    }

    public String getNombre() {        return nombre;    }

    public void setNombre(String nombre) {        this.nombre = nombre; }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public int getCodigoDeTipo() {
        return codigoDeTipo;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public void setCodigoDeTipo(int codigo) {
        this.codigoDeTipo = codigo;
    }

    public static final String CREAR_TABLA_ARCHIVO = "CREATE TABLE archivo ( nombre TEXT, curso TEXT, codigodetipo INTEGER, foto BLOB ) ";
    public static final String DROPEAR_TABLA_ARCHIVO = "DROP TABLE IF EXISTS archivo";

    public static final String TABLA_ARCHIVO = "archivo";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_CURSO = "curso";
    public static final String CAMPO_CODIGODETIPO = "codigodetipo";
    public static final String CAMPO_FOTO = "foto";


}
