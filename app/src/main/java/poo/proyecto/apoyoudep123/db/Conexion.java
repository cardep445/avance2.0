package poo.proyecto.apoyoudep123.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Conexion extends SQLiteOpenHelper {

    public static final String BD_ARCHIVOS = "bd_archivos";
    public static final String BD_USUARIOS = "bd_usuarios";

    public Conexion(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, BD_ARCHIVOS, null, 1);

    }
    public Conexion(@Nullable Context context) {
        super(context, BD_USUARIOS, null, 1);
    }





    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Archivo.CREAR_TABLA_ARCHIVO);
        db.execSQL(Usuario.CREAR_TABLA_USUARIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(Archivo.DROPEAR_TABLA_ARCHIVO);
        onCreate(db);
        db.execSQL(Usuario.DROPEAR_TABLA_USUARIO);

    }
}



