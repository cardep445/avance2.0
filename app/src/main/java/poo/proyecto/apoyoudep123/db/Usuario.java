package poo.proyecto.apoyoudep123.db;

import java.io.Serializable;

public class Usuario implements Serializable {

    private  String usuario;
    private String contrasena;
    private  String correo;
    private  String facultad;
    private String nombre;

    public Usuario(String usuario, String contrasena, String correo, String facultad, String nombre) {
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.correo = correo;
        this.facultad = facultad;
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public  String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public static final String CREAR_TABLA_USUARIO = "CREATE TABLE Usuarios (usuario TEXT, contrasena TEXT, correo TEXT, facultad TEXT, nombre TEXT)";
    public static final String TABLA_USUARIOS = "Usuarios";
    public static final String CAMPO_USUARIO = "usuario";
    public static final String CAMPO_CONTRASENA = "contrasena";
    public static final String CAMPO_CORREO = "correo";
    public static final String CAMPO_FACULTAD = "facultad";
    public static final String CAMPO_NOMBRE = "nombre";

    public static final String DROPEAR_TABLA_USUARIO = "DROP TABLE IF EXISTS Usuarios";


}
