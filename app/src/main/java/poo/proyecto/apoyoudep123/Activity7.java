package poo.proyecto.apoyoudep123;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import poo.proyecto.apoyoudep123.db.Conexion;
import poo.proyecto.apoyoudep123.db.Usuario;

public class Activity7 extends AppCompatActivity implements View.OnClickListener {
    EditText registroUsuario, registroContrasena, registroContrasena2,inputCorreo, registroNombre, registroFacultad;
    Button blisto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_7);

        registroUsuario = findViewById(R.id.registroUsuario);
        registroContrasena = findViewById(R.id.registroContraseña);
        registroContrasena2 = findViewById(R.id.registroContraseña2);
        inputCorreo = findViewById(R.id.inputCorreo);

        registroNombre = findViewById(R.id.registroNombre);
        registroFacultad = findViewById(R.id.registroFacultad);


        blisto= (Button) findViewById(R.id.botonlisto);
        blisto.setOnClickListener(this);


    }

    public void onClick(View v) {
        if (v.getId() == R.id.botonlisto) {

            String inputusuario = registroUsuario.getText().toString();
            String inputcontrasena = registroContrasena.getText().toString();
            String inputcontrasena2 = registroContrasena2.getText().toString();

            if (inputusuario.equals("")) {
                Toast.makeText(this, getString(R.string.problemaRegistroUsuario), Toast.LENGTH_SHORT).show();
            }else{

                if (inputcontrasena.equals("")) {
                    Toast.makeText(this, getString(R.string.problemaRegistroContrasena), Toast.LENGTH_SHORT).show();
                }else{

                    if (!inputcontrasena2.equals(inputcontrasena)) {
                        Toast.makeText(this, getString(R.string.problemaRegistroContrasena2), Toast.LENGTH_SHORT).show();
                    } else {


                        Conexion conexion = new Conexion(this);
                        SQLiteDatabase db = conexion.getWritableDatabase();

                        ContentValues values = new ContentValues();
                        values.put(Usuario.CAMPO_USUARIO, registroUsuario.getText().toString());
                        values.put(Usuario.CAMPO_CONTRASENA, registroContrasena.getText().toString());
                        values.put(Usuario.CAMPO_CORREO, inputCorreo.getText().toString());
                        values.put(Usuario.CAMPO_NOMBRE, registroNombre.getText().toString());
                        values.put(Usuario.CAMPO_FACULTAD, registroFacultad.getText().toString());


                        db.insert(Usuario.TABLA_USUARIOS,Usuario.CAMPO_USUARIO, values);
                        db.close();

                        Intent i = new Intent(this, Activity5.class);
                        startActivity(i);
                    }
                }
            }
        }

    }
}
