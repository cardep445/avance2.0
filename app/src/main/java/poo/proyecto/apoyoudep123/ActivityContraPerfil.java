package poo.proyecto.apoyoudep123;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import poo.proyecto.apoyoudep123.db.Conexion;
import poo.proyecto.apoyoudep123.db.Usuario;

public class ActivityContraPerfil extends AppCompatActivity implements View.OnClickListener {


    Button bConfirmar, bEliminarCuenta;
    EditText campoUsuario, anteriorContrasena, nuevaContrasena, nuevaContrasena2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contra_perfil);

        campoUsuario = (EditText) findViewById((R.id.campoUsuario));

        nuevaContrasena = (EditText) findViewById((R.id.nuevaContrasena));
        nuevaContrasena2 = (EditText) findViewById((R.id.nuevaContrasena2));

        bConfirmar = (Button) findViewById((R.id.confirmar));
        bConfirmar.setOnClickListener(this);

        bEliminarCuenta = (Button) findViewById(R.id.eliminarCuenta);
        bEliminarCuenta.setOnClickListener(this);
    }

    Conexion conexion = new Conexion(this);

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.confirmar) {

            SQLiteDatabase db = conexion.getWritableDatabase();
            String[] idUsuario = {campoUsuario.getText().toString()};

            if (nuevaContrasena.getText().toString().equals(nuevaContrasena2.getText().toString())) {

                ContentValues values = new ContentValues();
                values.put(Usuario.CAMPO_CONTRASENA, nuevaContrasena.getText().toString());

                db.update(Usuario.TABLA_USUARIOS, values, Usuario.CAMPO_USUARIO + "=?", idUsuario);

                Intent intent = new Intent(this, Activity5.class);
                startActivity(intent);
                db.close();
            }

        }



        if (v.getId() == R.id.eliminarCuenta){

            SQLiteDatabase db = conexion.getWritableDatabase();
            String[] idUsuario = {campoUsuario.getText().toString()};

            db.delete(Usuario.TABLA_USUARIOS, Usuario.CAMPO_USUARIO + "=?", idUsuario);

            Intent i = new Intent(this,Activity5.class);
            startActivity(i);

            db.close();
        }
    }
}