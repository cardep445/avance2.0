package poo.proyecto.apoyoudep123;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;


public class Activity4 extends AppCompatActivity implements  View.OnClickListener {
    Button bcancelar2, bmaterial,bresumen,bpcpasada;

    String curso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);


        bmaterial=(Button)findViewById(R.id.button);
        bmaterial.setOnClickListener(this);




        bcancelar2=(Button)findViewById(R.id.bcancelar23);
        bcancelar2.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.bcancelar23) {
            Intent i = new Intent(this, MainActivityfalse.class);
            startActivity(i);
        }
        if(v.getId()== R.id.button) {
            Intent i = new Intent(this, ActivityBMaterial.class);
            startActivity(i);
        }

    }
}
