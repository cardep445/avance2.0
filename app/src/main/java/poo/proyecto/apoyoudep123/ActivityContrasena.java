package poo.proyecto.apoyoudep123;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import poo.proyecto.apoyoudep123.db.Conexion;
import poo.proyecto.apoyoudep123.db.Usuario;

public class ActivityContrasena extends AppCompatActivity implements View.OnClickListener {


    Button bConfirmar;
    EditText campoUsuario, anteriorContrasena, nuevaContrasena, nuevaContrasena2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contrasena);

        campoUsuario = (EditText) findViewById((R.id.campoUsuario));

        nuevaContrasena = (EditText) findViewById((R.id.nuevaContrasena));
        nuevaContrasena2 = (EditText) findViewById((R.id.nuevaContrasena2));

        bConfirmar = (Button) findViewById((R.id.confirmar));
        bConfirmar.setOnClickListener(this);

    }

    Conexion conexion = new Conexion(this);

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.confirmar) {

            SQLiteDatabase db = conexion.getWritableDatabase();
            String[] idUsuario = {campoUsuario.getText().toString()};

            if (nuevaContrasena.getText().toString().equals(nuevaContrasena2.getText().toString())) {

                ContentValues values = new ContentValues();
                values.put(Usuario.CAMPO_CONTRASENA, nuevaContrasena.getText().toString());

                db.update(Usuario.TABLA_USUARIOS, values, Usuario.CAMPO_CONTRASENA + "=?", idUsuario);

                Intent intent = new Intent(this, Activity5.class);
                startActivity(intent);
                db.close();
            }

            }
    }
}